#include <stdint.h>
#include <stdbool.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef VECTOR_H
#define VECTOR_H

typedef bool bvec1_t[1];
typedef bool bvec2_t[2];
typedef bool bvec3_t[3];
typedef bool bvec4_t[4];
typedef int8_t i8vec1_t[1];
typedef int8_t i8vec2_t[2];
typedef int8_t i8vec3_t[3];
typedef int8_t i8vec4_t[4];
typedef int16_t i16vec1_t[1];
typedef int16_t i16vec2_t[2];
typedef int16_t i16vec3_t[3];
typedef int16_t i16vec4_t[4];
typedef int32_t i32vec1_t[1];
typedef int32_t i32vec2_t[2];
typedef int32_t i32vec3_t[3];
typedef int32_t i32vec4_t[4];
typedef int64_t i64vec1_t[1];
typedef int64_t i64vec2_t[2];
typedef int64_t i64vec3_t[3];
typedef int64_t i64vec4_t[4];
typedef uint8_t u8vec1_t[1];
typedef uint8_t u8vec2_t[2];
typedef uint8_t u8vec3_t[3];
typedef uint8_t u8vec4_t[4];
typedef uint16_t u16vec1_t[1];
typedef uint16_t u16vec2_t[2];
typedef uint16_t u16vec3_t[3];
typedef uint16_t u16vec4_t[4];
typedef uint32_t u32vec1_t[1];
typedef uint32_t u32vec2_t[2];
typedef uint32_t u32vec3_t[3];
typedef uint32_t u32vec4_t[4];
typedef uint64_t u64vec1_t[1];
typedef uint64_t u64vec2_t[2];
typedef uint64_t u64vec3_t[3];
typedef uint64_t u64vec4_t[4];
typedef float f32vec1_t[1];
typedef float f32vec2_t[2];
typedef float f32vec3_t[3];
typedef float f32vec4_t[4];
typedef double f64vec1_t[1];
typedef double f64vec2_t[2];
typedef double f64vec3_t[3];
typedef double f64vec4_t[4];

#endif
