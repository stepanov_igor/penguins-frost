#include <stdint.h>
#include <stdbool.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "vector.h"
#include "engine.h"
#include <SDL2/SDL.h>
#include <time.h>
//
//typedef struct object_t object_t;
//
//struct object_t
//{
//    f32vec2_t *basis;
//    uint32_t   basis_count;
//    f32vec2_t *world;
//    uint32_t   world_count;
//
//    f32vec2_t min;
//    f32vec2_t max;
//
//    f32vec1_t corner;
//    f32vec1_t corner_delta;
//    f32vec1_t corner_speed;
//    f32vec1_t corner_boost;
//    f32vec2_t center;
//    f32vec2_t center_delta;
//    f32vec2_t center_speed;
//    f32vec2_t center_boost;
//};
//
//void object_update(object_t *object, float time)
//{
//    object->corner_speed[0] += object->corner_boost[0] * time;
//    object->center_speed[0] += object->center_boost[0] * time;
//    object->center_speed[1] += object->center_boost[1] * time;
//    object->corner_delta[0] = object->corner_speed[0] * time;
//    object->center_delta[0] = object->center_speed[0] * time;
//    object->center_delta[1] = object->center_speed[1] * time;
//    object->corner[0] += object->corner_delta[0];
//    object->center[0] += object->center_delta[0];
//    object->center[1] += object->center_delta[1];
//
//    object->min[0] = FLT_MAX;
//    object->min[1] = FLT_MAX;
//    object->max[0] = FLT_MIN;
//    object->max[1] = FLT_MIN;
//
//    for(int index = 0; index < object->basis_count; index += 1)
//    {
//        f32vec2_t copy;
//
//        copy[0] = object->basis[index][0];
//        copy[1] = object->basis[index][1];
//
//        object->world[index][0] = cos(object->corner[0]) * copy[0] - sin(object->corner[0]) * copy[1];
//        object->world[index][1] = sin(object->corner[0]) * copy[0] + cos(object->corner[0]) * copy[1];
//
//        object->world[index][0] += object->center[0];
//        object->world[index][1] += object->center[1];
//
//        object->min[0] = (object->min[0] > object->world[index][0]) ? object->world[index][0] : object->min[0];
//        object->min[1] = (object->min[1] > object->world[index][1]) ? object->world[index][1] : object->min[1];
//        object->max[0] = (object->max[0] < object->world[index][0]) ? object->world[index][0] : object->max[0];
//        object->max[1] = (object->max[1] < object->world[index][1]) ? object->world[index][1] : object->max[1];
//    }
//}
//
//void star(object_t *object, uint32_t radius, uint32_t number)
//{
//    object->basis = malloc(sizeof(f32vec2_t) * number);
//    object->world = malloc(sizeof(f32vec2_t) * number);
//    for(int index = 0; index < number; index += 1)
//    {
//        float angle = 2 * 3.14 / number * index;
//        float x = radius * cos(angle);
//        float y = radius * sin(angle);
//        object->basis[index][0] = x;
//        object->basis[index][1] = y;
//    }
//    object->basis_count = number;
//    object->world_count = number;
//}
//
//
//
//cairo_surface_t* create_x11_surface(Display *d, int x, int y)
//{
//    Drawable da;
//    int screen;
//    cairo_surface_t* sfc;
//
//    screen = DefaultScreen(d);
//    da = XCreateSimpleWindow(d, DefaultRootWindow(d), 0, 0, x, y, 0, 0, 0);
//    XSelectInput(d, da, ButtonPressMask | KeyPressMask);
//    XMapWindow(d, da);
//
//    sfc = cairo_xlib_surface_create(d, da, DefaultVisual(d, screen), x, y);
//
//    return sfc;
//}

////    object_t *obj1;
////    obj1 = malloc(sizeof(object_t));
////    memset(obj1, 0, sizeof(object_t));
////    obj1->center[0] = 0.0;
////    obj1->center[1] = 0.0;
////    obj1->center_speed[0] = 0.00000001;
////    obj1->center_speed[1] = 0.00000001;
////    obj1->corner_speed[0] = 0.000000001;
////    star(obj1, 50, 3);
////
////    Display *d = XOpenDisplay(NULL);
////    cairo_surface_t* surface = create_x11_surface(d, 1000, 1000);
////    cairo_t* cr = cairo_create(surface);
////
////    struct timespec time = {0, 0};
////    uint32_t time_next = 0;
////    uint32_t time_last = 0;
////    uint32_t time_step = 0;
////    uint32_t time_wake = 0;
////
////    while(1)
////    {
////
////        clock_gettime(CLOCK_REALTIME, &time);
////        time_last = time.tv_sec * 1000000000 + time.tv_nsec;
////
////        // Clear the background
////        cairo_set_source_rgb(cr, 1, 1, 1);
////        cairo_paint(cr);
////
////        object_update(obj1, (float)time_step);
////        cairo_set_source_rgb(cr, 1, 0, 0);
////        cairo_set_line_width(cr, 2.0);
////        for(int index = 0; index < obj1->basis_count; index += 1)
////        {
////            f32vec2_t last;
////            f32vec2_t next;
////            last[0] = obj1->world[(index + 0) % obj1->basis_count][0];
////            last[1] = obj1->world[(index + 0) % obj1->basis_count][1];
////            next[0] = obj1->world[(index + 1) % obj1->basis_count][0];
////            next[1] = obj1->world[(index + 1) % obj1->basis_count][1];
////            cairo_move_to(cr, last[0], last[1]);
////            cairo_line_to(cr, next[0], next[1]);
////        }
////        cairo_set_line_width(cr, 1.0);
////        cairo_move_to(cr, obj1->min[0], obj1->max[1]);
////        cairo_line_to(cr, obj1->max[0], obj1->max[1]);
////        cairo_move_to(cr, obj1->max[0], obj1->max[1]);
////        cairo_line_to(cr, obj1->max[0], obj1->min[1]);
////        cairo_move_to(cr, obj1->max[0], obj1->min[1]);
////        cairo_line_to(cr, obj1->min[0], obj1->min[1]);
////        cairo_move_to(cr, obj1->min[0], obj1->min[1]);
////        cairo_line_to(cr, obj1->min[0], obj1->max[1]);
////        cairo_stroke(cr);
////        cairo_surface_flush(surface);
////        XFlush(d);
////
////        clock_gettime(CLOCK_REALTIME, &time);
////        time_next = time.tv_sec * 1000000000 + time.tv_nsec;
////        time_step = time_next - time_last;
////        time_wake = 1000000000 / 60 - time_step + time_next;
////        struct timespec wake = {time_wake / 1000000000 - time_wake % 1000000000, time_wake % 1000000000};
////        clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &wake, NULL);
////
////    }
////    cairo_surface_destroy(surface);

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

void create()
{
	SDL_Init( SDL_INIT_EVERYTHING );
	window = SDL_CreateWindow( "Engine", 80, 80, 1024, 768, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
}

void delete()
{
	SDL_DestroyRenderer( renderer );
	renderer = NULL;
	SDL_DestroyWindow( window );
	window = NULL;
	SDL_Quit();
}

int main(int argc, char** argv)
{
    create();

    delete();
}
